
#pragma once
#include "SpriteObject.h"
#include "Player.h"

class Game;

class ExitE : public SpriteObject
{
public:
	// Constructors / Destructors
	ExitE(sf::Vector2f startingPos, Game* NewLvlPointer);
	void HandleSolidCollision(Player& Playerobject);
	// Functions to call Player-specific code



private:
	int LvlNumber;
	Game* LvlPointer;

};