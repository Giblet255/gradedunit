#include "TreeT.h"
#include "AssetManager.h"



TreeT::TreeT(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/TreeT.png"))
{
	sprite.setPosition(startingPos);
}