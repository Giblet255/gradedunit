#include "Player.h"
#include "game.h"
#include "AssetManager.h"
#include <cmath>
#include <SFML/Graphics.hpp>


Player::Player()
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation2.png"), 109, 142, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(350.0f)
	, previousPosition()
	, iskeypressed()
	, iskeyreleased()
	, WarpCooldownRemaining(sf::seconds(0.0f))
	, WarpCooldownMax(sf::seconds(0.5f))
	, PlayerHP(225) // setting starting health 
	, PlayerMagic(242) // Setting Mana This number a max Health and magic
	
		


{

	
	//sprite.setScale(100, 100);

	AddClip("WalkRight", 0, 9);
	AddClip("WalkLeft", 10, 19);
	AddClip("WalkDown", 20, 29);
	AddClip("WalkUp", 30, 39);
	AddClip("StandDown", 40, 49);
	AddClip("StandRight", 50, 57);
	AddClip("PWRSlamright", 60, 69);
	AddClip("Attack", 80, 85);
	// Set player looking Right
	PlayClip("StandRight",true);
	
	
}


Hud::Hud(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/HUDfrm.png"))
	, hudloc()
{
	sprite.setPosition(startingPos);
	
}



void Hud::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}

void Player::Input()
{

	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;




	bool hasInput = false;
	bool NoKeyPress = false;
	bool JoyButtonP = true;
	bool JoybuttonWP = false;

	
	
	





	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
		if (!hasInput)
		{
			PlayClip("WalkUp", false);
			NoKeyPress = true;
		}

		hasInput = true;

	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
		if (!hasInput)
		{
			PlayClip("WalkLeft", false);
			NoKeyPress = true;
		}

		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
		if (!hasInput)
		{
			PlayClip("WalkDown", false);
			NoKeyPress = true;
		}

		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight", false);
			NoKeyPress = true;

		}

		hasInput = true;

	}



	// Added ArrowKeys 

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		// Move player up
		velocity.y = -speed;
		if (!hasInput)
		{
			PlayClip("WalkUp", false);
			NoKeyPress = true;

		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		// Move player left
		velocity.x = -speed;
		if (!hasInput)
		{
			PlayClip("WalkLeft", false);
			NoKeyPress = true;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		// Move player down
		velocity.y = speed;
		if (!hasInput)
		{
			PlayClip("WalkDown", false);
			NoKeyPress = true;

		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight", false);
			NoKeyPress = true;
			iskeyreleased = false;

		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight", false);
			NoKeyPress = true;


		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && WarpCooldownRemaining <= sf::seconds(0.0f))
	{
		if (PlayerMagic > 0)
		{



			sf::Vector2f Offest(50, 30);
			sf::Vector2f KTRight(600, 0);
			sf::Vector2f KTLeft(-600, 0);
			sf::Vector2f KTNone(0, 0);



			// Call Warp atm need to change to attackcool down 
			WarpCooldownRemaining = WarpCooldownMax;


			PlayClip("Attack", false);
			NoKeyPress = true;

			// This is setting where the knife is thrown form and setting the speed it moves 
			playerKifeSprite.setPosition(sprite.getPosition() + Offest);



			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				playerKifeVel = KTLeft;
				playerKifeTexture.loadFromFile("Assets/Graphics/PKnifeLeft.png");
				playerKifeSprite.setTexture(playerKifeTexture);
				PlayerMagic = PlayerMagic - 50;
			}
			else
			{

				playerKifeVel = KTRight;
				playerKifeTexture.loadFromFile("Assets/Graphics/PKnifeRight.png");
				playerKifeSprite.setTexture(playerKifeTexture);
				PlayerMagic = PlayerMagic - 50;
			}



		}

	}

	


	if (sf::Joystick::isConnected(0))
	{

		if (sf::Joystick::isButtonPressed(0, 5))
		{
			JoybuttonWP = true;
			JoyButtonP = false;
		}
		

		// what's the current position of the X and Y axes of joystick number 0?
		float x = sf::Joystick::getAxisPosition(0, sf::Joystick::X);
		float y = sf::Joystick::getAxisPosition(0, sf::Joystick::Y);

		if (x > 20)
		{
			// This the the back button on joystick this will exit game Soon
			velocity.x = speed;
			if (!hasInput)
			{
				PlayClip("WalkRight", false);
				NoKeyPress = true;

			}
		}

		if (x < -20)
		{
			// Move player left
			velocity.x = -speed;
			if (!hasInput)
			{
				PlayClip("WalkLeft", false);
				NoKeyPress = true;
			}
		}

		if (y > 20)
		{
			// Move player down
			velocity.y = speed;
			if (!hasInput)
			{
				PlayClip("WalkDown", false);
				NoKeyPress = true;

			}
		}

		if (y < -20)
		{
			// Move player up
			velocity.y = -speed;
			if (!hasInput)
			{
				PlayClip("WalkUp", false);
				NoKeyPress = true;

			}
		}

		if (sf::Joystick::isButtonPressed(0, 0))
		{
			
				if (PlayerMagic > 0)
				{

					sf::Vector2f Offest(50, 30);
					sf::Vector2f KTRight(600, 0);
					sf::Vector2f KTLeft(-600, 0);
					sf::Vector2f KTNone(0, 0);



					// Call Warp atm need to change to attackcool down 
					WarpCooldownRemaining = WarpCooldownMax;


					PlayClip("Attack", false);
					NoKeyPress = true;

					// This is setting where the knife is thrown form and setting the speed it moves 
					playerKifeSprite.setPosition(sprite.getPosition() + Offest);



					if (x < -20)
					{
						playerKifeVel = KTLeft;
						playerKifeTexture.loadFromFile("Assets/Graphics/PKnifeLeft.png");
						playerKifeSprite.setTexture(playerKifeTexture);
						//PlayerMagic = PlayerMagic - 50;
					}
					else
					{

						playerKifeVel = KTRight;
						playerKifeTexture.loadFromFile("Assets/Graphics/PKnifeRight.png");
						playerKifeSprite.setTexture(playerKifeTexture);
						//PlayerMagic = PlayerMagic - 50;
					}



				}

			
		}

	}


	if (!NoKeyPress && WarpCooldownRemaining < sf::seconds(0.0f))
	{
		PlayClip("StandRight", false);
		NoKeyPress = false;


	}



}

void Player::Update(sf::Time frameTime)
{
	
	

	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	// this is get prevous pot you need this to make player jump back
	previousPosition = sprite.getPosition();

	// Move the player to the new position
	sprite.setPosition(newPosition);


	// Update Knife position
	
	
	

	playerKifeSprite.setPosition(playerKifeSprite.getPosition() + playerKifeVel * frameTime.asSeconds());


	AnimatingObject::Update(frameTime);

	// Update the cooldown remaining for Warp move
	WarpCooldownRemaining -= frameTime;
	PlayerMagic = PlayerMagic + 1 ;

	

}

void Player::DrawTo(sf::RenderTarget& target)
{
	
	target.draw(playerKifeSprite);
	target.draw(sprite);
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	// Check if there is actually a collision happening
	bool isColliding = GetHitbox().intersects(otherHitbox);
	// If there is a collision...
	if (isColliding)
	{
		// TODO: Calculate the collision depth (overlap)
		sf::Vector2f depth = CalculateCollisionDepth(otherHitbox);
		// TODO: Determine which is smaller - the x or y overlap
		// Determine which is smaller - the x or y overlap
		sf::Vector2f newPosition = sprite.getPosition();
		if (std::abs(depth.x) < std::abs(depth.y))
		{
			// Calculate a new x coordinate such that the objects don't overlap
			newPosition.x -= depth.x;
		}
		else
		{
			// TODO: Calculate a new y coordinate such that the objects don't overlap
			newPosition.y -= depth.y;
		}

		// TODO: Move the sprite by the depth in whatever direction wassmaller		// Move the sprite by the depth in whatever direction was smaller
		sprite.setPosition(newPosition);

		

		
	}	
	// Player This Stop Health bar getting to low
	if (PlayerHP < 0)
	{
		PlayerHP = 0;
	}

	if (PlayerHP > 230)
	{
		PlayerHP = 230;
	}

	if (PlayerMagic < -4)
	{
		PlayerMagic = -4;
	}

	if (PlayerMagic > 240)
	{
		PlayerMagic = 240;
	}
}

void Player::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
	
}

void Player::SetPlayerHP(int newhealth)
{
	PlayerHP = newhealth;
}

int Player::GetPlayerHP()
{
	return PlayerHP ;
}

int Player::GetPlayerMagic()
{
	return PlayerMagic;
}

sf::Vector2f Player::GetplayerCurpos()
{
	playerCurPOs = sprite.getPosition();
	return sf::Vector2f(playerCurPOs);
}

sf::Vector2f Player::GetPKCurpos()
{
	PKCurPOs = playerKifeSprite.getPosition();
	return sf::Vector2f(PKCurPOs);
}


