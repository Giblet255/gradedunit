#pragma once
#include "SpriteObject.h"
#include "Player.h"

class Game;

class ExitT : public SpriteObject
{
public:
	// Constructors / Destructors
	ExitT(sf::Vector2f startingPos,Game* NewLvlPointer) ;
	void HandleSolidCollision(Player& Playerobject);
	// Functions to call Player-specific code
	


private:
	int LvlNumber;
	Game* LvlPointer;

};