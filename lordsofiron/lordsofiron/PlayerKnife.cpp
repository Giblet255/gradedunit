#include "PlayerKnife.h"
#include "AssetManager.h"


PlayerKnife::PlayerKnife(sf::Texture& PlayerKnifeTexture, sf::Vector2u newScreenBounds, sf::Vector2f startingPos, sf::Vector2f newVelocity)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/PKinfe.png"))
{
	
	screenBounds = newScreenBounds;
	sprite.setPosition(startingPos);
	velocity = newVelocity;
	alive = true;
}
bool PlayerKnife::GetAlive()
{
	return alive;
}


void PlayerKnife::Update(sf::Time frameTime)
{
	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	// Have we gone off the screen completely to the left or right?
	if (newPosition.x + sprite.getTexture()->getSize().x < 0
		|| newPosition.x > screenBounds.x)
	{
		// TODO: Our PlayerKnife is off the screen - we should delete it

	}
	// Move to the new position
	sprite.setPosition(newPosition);
}

void PlayerKnife::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}


sf::FloatRect PlayerKnife::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void PlayerKnife::SetAlive(bool newAlive)
{
	alive = newAlive;
}