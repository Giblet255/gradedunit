#pragma once
#include "AnimatingObject.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp> // this load in the sound buffers
#include <vector> // Library for handling collections of objects



class Player : public AnimatingObject
{
public:
	// Constructors / Destructors
	Player();
	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
	void HandleSolidCollision(sf::FloatRect otherHitbox);
	void SetPosition(sf::Vector2f newPosition);
	void SetPlayerHP(int newhealth);
	int GetPlayerHP();//this is a gett you idiot please dont forget 
	int GetPlayerMagic(); // this is a getter for magic
	sf::Vector2f GetplayerCurpos();
	sf::Vector2f GetPKCurpos();
	
	

	
	
private:
	// Data
	sf::Vector2f velocity;
	float speed;
	//sf::Vector2f previousPosition;
	bool iskeypressed = false;
	bool iskeyreleased = true;
	sf::Time WarpCooldownRemaining;
	sf::Time WarpCooldownMax;
	sf::Vector2f playerCurPOs;
	sf::Vector2f previousPosition;
	int PlayerHP = 225;
	int PlayerMagic;

	sf::Vector2f PKStartPos;
	sf::Vector2f playerKifeVel;
	sf::Vector2f PKCurPOs;
	
	

	// PlayerKnife Setup 
	sf::Texture playerKifeTexture;
	sf::Sprite playerKifeSprite;
	
	

	

	
};

class Hud : public SpriteObject
{
public:
	// Constructors / Destructors
	Hud(sf::Vector2f startingPos);
	// Functions to call Player-specific code
	void SetPosition(sf::Vector2f newPosition);

private:
	sf::Vector2f hudloc;


};