#include "SpriteObject.h"

SpriteObject::SpriteObject(sf::Texture & newTexture)
	:sprite(newTexture)
{


}

void SpriteObject::DrawTo(sf::RenderTarget & target)
{
	target.draw(sprite);
}

sf::FloatRect SpriteObject::GetHitbox()
{
	return sprite.getGlobalBounds();
}

sf::Vector2f SpriteObject::CalculateCollisionDepth(sf::FloatRect otherHitbox)
{
	// Colloisn Detp 

	sf::FloatRect thisHitbox = GetHitbox();
	// Calculate the center of each object
	sf::Vector2f centerOther(otherHitbox.left + otherHitbox.width / 2.0f, otherHitbox.top + otherHitbox.height / 2.0f);
	sf::Vector2f centerThis(thisHitbox.left + thisHitbox.width / 2.0f, thisHitbox.top + thisHitbox.height / 2.0f);
	// Calculate the current distance between the two objects
	float distanceX = centerOther.x - centerThis.x;
	float distanceY = centerOther.y - centerThis.y;	// Calculate the minimum collision distance between the twoobjects
	float minDistanceX = otherHitbox.width / 2.0f + thisHitbox.width / 2.0f;
	float minDistanceY = otherHitbox.height / 2.0f + thisHitbox.height / 2.0f;
	// Account for negative positioning
	if (distanceX < 0)
		minDistanceX = -minDistanceX;
	if (distanceY < 0)
		minDistanceY = -minDistanceY;

	// Calculate the depth by subtracting distance from minimumdistance
	float depthX = minDistanceX - distanceX;
	float depthY = minDistanceY - distanceY;
	// Return the calculated depth to the calling code
	return sf::Vector2f(depthX, depthY);
}
