#include "Badguyz.h"
#include "Player.h"
#include "AssetManager.h"
#include <cmath>
#include <SFML/Graphics.hpp>


BadGuyz::BadGuyz(sf::Vector2f startingPos)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/Badguyz1.png"), 109, 142, 7.0f)
	, velocity(0.0f, 0.0f)
	, speed(150.0f)
	, previousPosition()
	, locpointA()
	, locpointB()
	, locpointC()
	, movementPattern()
	, currentInstruction(0)
	, alive(true)
{
	

	AddClip("BGWalkRight", 0, 9);
	AddClip("BGWalkLeft", 11, 19);
	AddClip("BGWalkDown", 21, 29);
	AddClip("BGWalkUp", 31, 39);
	AddClip("BGDieLeft", 41, 46);
	AddClip("BGDieRight", 51,59);
	// Set player looking Right
	PlayClip("BGWalkLeft", true);

	
	sprite.setPosition(startingPos);
	
	// Movment patterns 
	// Setting is so that each bad guyz will have a diffent start point so i am using this to start the movment patter
	locpointA.y = startingPos.y;
	locpointA.x = startingPos.x + 200;
	

	locpointB.y = locpointA.y;
	locpointB.x = locpointA.x - 400;
	

	locpointC.y = locpointB.y ;
	locpointC.x = locpointB.x + 400;

	
	movementPattern.push_back(sf::Vector2f(locpointA));
	movementPattern.push_back(sf::Vector2f(locpointB));
	movementPattern.push_back(sf::Vector2f(locpointC));
	
	
	

}



void BadGuyz::Update(sf::Time frameTime,Player &playerObject)
{
	// Interpolation data
	sf::Vector2f begin ;
	sf::Vector2f end ;
	sf::Vector2f PausePosition;
	sf::Vector2f change = end - begin;
	float deltaTime = frameTime.asSeconds();
	float duration = 1.0f;
	float time = 0.0f;
	
	

	// this stop the index justing above the array (vector)
	// This Section is making the movment patter over and over again 
	if (currentInstruction >= movementPattern.size())
	{
		
		PlayClip("BGStandRight", true);
		currentInstruction = 0; // this make the loop <-
		
	}

	// Get Tagert From instruction 

	sf::Vector2f targetPoint = movementPattern[currentInstruction];

	sf::Vector2f distanceVector = targetPoint - sprite.getPosition();



	// Call direction Vector (by maths)

	float distanceMag = std::sqrt(distanceVector.x*distanceVector.x + distanceVector.y*distanceVector.y);

	sf::Vector2f directionVector = distanceVector / distanceMag;

	float distancetoreavel = speed * frameTime.asSeconds();

	sf::Vector2f NewPosition = sprite.getPosition() + directionVector * distancetoreavel;


	
	// this is working out which where badguyz is face and playing correct clip for the movment 
	if (targetPoint.x < NewPosition.x)
	{
		PlayClip("BGWalkLeft", true);
	}
	if (targetPoint.x > NewPosition.x)
	{
		PlayClip("BGWalkRight", true);
	}
	if (targetPoint.y < NewPosition.y)
	{
		PlayClip("BGWalkUP", true);
	}
	if (targetPoint.y > NewPosition.y)
	{
		PlayClip("BGWalkDown", true);
	}
	

	// Adding A Chase Funtion i have set up a hidden Cirle around 
	// the player and bad guyz i am now setting a if statment to make them chase player

	// setting player Cirle
	float PlayerRad = 100;
	

	// setting player2 Cirle
	float Player2rad = 300;
	

	sf::Vector2f circledisplacment = playerObject.GetplayerCurpos() - sprite.getPosition();
	float SQUDistance = circledisplacment.x * circledisplacment.x + circledisplacment.y * circledisplacment.y;
	float SQUDRad = (PlayerRad + Player2rad) * (PlayerRad + Player2rad);

	bool inRange = SQUDistance < SQUDRad;

	if (inRange)
	{
	

			// Get Tagert From instruction 

			sf::Vector2f targetPoint = playerObject.GetplayerCurpos();

			sf::Vector2f distanceVector = targetPoint - sprite.getPosition();



			// Call direction Vector (by maths)

			float distanceMag = std::sqrt(distanceVector.x * distanceVector.x + distanceVector.y * distanceVector.y);

			sf::Vector2f directionVector = distanceVector / distanceMag;

			float distancetoreavel = speed * frameTime.asSeconds();

			NewPosition = sprite.getPosition() + directionVector * distancetoreavel;


			if (targetPoint.x < NewPosition.x)
			{
				PlayClip("BGWalkLeft", true);
			}
			if (targetPoint.x > NewPosition.x)
			{
				PlayClip("BGWalkRight", true);
			}
			

				

	}
	if (inRange == false)
	{

		// Check to see if we have reached target loc 

		if (distanceMag <= distancetoreavel)
		{

			NewPosition = targetPoint;
			++currentInstruction;

		}

		if (targetPoint.x < NewPosition.x)
		{
			PlayClip("BGWalkLeft", true);
		}
		if (targetPoint.x > NewPosition.x)
		{
			PlayClip("BGWalkRight", true);
		}
		
	}


	// this is working out if the knife hits 
	// 
	// setting player Cirle
	float PKnifeRad = 10;


	// setting player2 Cirle
	float Badguyrad = 70;


	sf::Vector2f circledisplacment2 = playerObject.GetPKCurpos() - sprite.getPosition();
	float SQUDistance2 = circledisplacment2.x * circledisplacment2.x + circledisplacment2.y * circledisplacment2.y;
	float SQUDRad2 = (PKnifeRad + Badguyrad) * (PKnifeRad + Badguyrad);

	bool inRange2 = SQUDistance2 < SQUDRad2;

	if (inRange2 && deathtimecooldown <= sf::seconds(0.0f))
	{
		
			
			PlayClip("BGDieRight", true);
			
			// reset DeathAnmationClock cooldown
			deathtimecooldown = deathtimemax;
			
			alive = false;

	}


	// Move the Ememy
	
	sprite.setPosition(NewPosition);
	
	
	
	// Update the cooldown remaining for Death anmation 
	deathtimecooldown -= frameTime;
	
	

	AnimatingObject::Update(frameTime);
	
}

void BadGuyz::HandleSolidCollision(Player &PlayerOjbect)
{
	

	//  #######################################################
	//  This is you pulling info for player you needed to make funting on the 
	//  player.cpp to do this then uses getters and setter to get them also 
	//  you call this funtoning in lvlscreen
	//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Check if there is actually a collision happening
	bool isColliding = GetHitbox().intersects(PlayerOjbect.GetHitbox());
	// If there is a collision...
	if (isColliding )
	{
		int playerHP; 
		int NewplayerHp;

		
		playerHP = PlayerOjbect.GetPlayerHP();

		NewplayerHp = playerHP - 1;

		PlayerOjbect.SetPlayerHP(NewplayerHp);


		
	}

	
	
}



bool BadGuyz::GetAlive()
{
	return alive;
}

void BadGuyz::SetAlive(bool newAlive)
{
	alive = newAlive;
}



