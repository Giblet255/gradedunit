#include "Game.h"
#include "LevelScreen.h"
#include "player.h"

Game::Game()
	//: window(sf::VideoMode::getDesktopMode(), "Lords Of Iron", sf::Style::Titlebar | sf::Style::Close)
	: window(sf::VideoMode::getDesktopMode(), "Lords Of Iron", sf::Style::Titlebar | sf::Style::Fullscreen)
	, gameClock()
	, levelScreenInstance(this)	, MenuOpt(0)	, LvlNumber(1)		
{
	// Window setup (Hidding Mouse)
	window.setFramerateLimit(60);
	window.setMouseCursorVisible(false);
	levelScreenInstance.LoadLevel(LvlNumber);
}

void Game::RunGameLoop()
{
	// Repeat as long as the window is open
	while (window.isOpen())
	{
		if (MenuOpt == 0) {
			Menu();
		}
		if (MenuOpt == 1) {
			Input();
			Update();
			Draw();
		}
		if (MenuOpt == 2) {
			Option();
		}
		if (MenuOpt == 5) {
			GameOver();
		}
		if (MenuOpt == 6) {
			GameWin();
		}
	}
}

void Game::Menu()
{
	// Setting up Font
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	int PlayCho = 0;
	int keypressed = 0;
	sf::Time frameTime = gameClock.restart();

	// Close game if escape is pressed
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))

	{
		window.close();
	}
	
	

	if (MenuOpt == 0)
	{
		

		// Main Menu BackGound img ------------------------
		sf::Texture bgTexture;
		bgTexture.loadFromFile("Assets/Graphics/menub.png");
		sf::Sprite BGImg;
		BGImg.setTexture(bgTexture);
		BGImg.setPosition(0, 0);

		// Setting Game Play menu option  ----------------------
		
		sf::Text PlayTxt;
		PlayTxt.setFont(gameFont);
		PlayTxt.setString("Press 'P' To Play");
		PlayTxt.setCharacterSize(30);
		PlayTxt.setPosition(150, 505);


		// Setting Game option menu txt ---------------------------

		sf::Text OptTxt;
		OptTxt.setFont(gameFont);
		OptTxt.setString("Press 'H' For Help");
		OptTxt.setCharacterSize(30);
		OptTxt.setPosition(150, 600);

		// Setting Game Quit menu txt ---------------------------

		sf::Text ExtTxt;
		ExtTxt.setFont(gameFont);
		ExtTxt.setString("Press 'Esc' To Quit");
		ExtTxt.setCharacterSize(25);
		ExtTxt.setPosition(150, 695);
			

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))

		{
		window.close();
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))

		{
			MenuOpt = 1;
			levelScreenInstance.LoadLevel(LvlNumber);
			
			
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))

		{
			MenuOpt = 2;
		}

		if (sf::Joystick::isConnected(0))
		{
			if (sf::Joystick::isButtonPressed(0, 0))
			{
				MenuOpt = 1;
				levelScreenInstance.LoadLevel(LvlNumber);
			}

			if (sf::Joystick::isButtonPressed(0, 1))
			{
				MenuOpt = 2;
			}
			if (sf::Joystick::isButtonPressed(0, 3))
			{
				MenuOpt = 0;
			}


		}
	


		// THIS CLEAR SREEN AND ADD COLOUR
		window.clear(sf::Color(0, 0, 0));
		// Drawing BackGound Image 
		window.draw(BGImg);
		window.draw(PlayTxt);
		window.draw(OptTxt);
		window.draw(ExtTxt);
		window.display();


	}

	
	
}

void Game::Option()
{
	// Main Menu BackGound img ------------------------
	sf::Texture bgTexture;
	bgTexture.loadFromFile("Assets/Graphics/menuOptionsb.png");
	sf::Sprite BGImg;
	BGImg.setTexture(bgTexture);
	BGImg.setPosition(0, 0);

	// Setting Game Play menu option  ----------------------
	// Setting up Font
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	sf::Text PlayTxt;
	PlayTxt.setFont(gameFont);
	PlayTxt.setString("Press 'P' To Play");
	PlayTxt.setCharacterSize(30);
	PlayTxt.setPosition(150, 505);

	// Setting Game option menu txt ---------------------------

	sf::Text OptTxt;
	OptTxt.setFont(gameFont);
	OptTxt.setString("Press 'R' Return");
	OptTxt.setCharacterSize(30);
	OptTxt.setPosition(150, 600);

	// Setting Game Quit menu txt ---------------------------

	sf::Text ExtTxt;
	ExtTxt.setFont(gameFont);
	ExtTxt.setString("Press 'Esc' To Quit");
	ExtTxt.setCharacterSize(25);
	ExtTxt.setPosition(150, 695);


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))

	{
		window.close();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))

	{
		MenuOpt = 0;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))

	{
		MenuOpt = 1;
		
		
		
	}


	// THIS CLEAR SREEN AND ADD COLOUR
	window.clear(sf::Color(0, 0, 0));
	// Drawing BackGound Image 
	window.draw(BGImg);
	window.draw(PlayTxt);
	window.draw(OptTxt);
	window.draw(ExtTxt);
	window.display();




}

void Game::GameOver()
{
	// Game Over BackGound img ------------------------
	sf::Texture bgTexture;
	bgTexture.loadFromFile("Assets/Graphics/Gameover.png");
	sf::Sprite BGImg;
	BGImg.setTexture(bgTexture);
	BGImg.setPosition(0, 0);
	// --------------------------
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	sf::Text GameOverTxt;
	GameOverTxt.setFont(gameFont);
	GameOverTxt.setString("Please press 'R' to Restart Game...");
	GameOverTxt.setCharacterSize(40);
	GameOverTxt.setPosition(200, 505);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))

	{
		MenuOpt = 0;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))

	{
		window.close();
	}

	// THIS CLEAR SREEN AND ADD COLOUR
	window.clear(sf::Color(255, 0, 0));
	// Drawing BackGound Image 
	window.draw(BGImg);
	window.draw(GameOverTxt);
	window.display();

}

void Game::GameWin()
{
	// Game Winner BackGound img ------------------------
	sf::Texture bgTexture;
	bgTexture.loadFromFile("Assets/Graphics/Gamewin3.png");
	sf::Sprite BGImg;
	BGImg.setTexture(bgTexture);
	BGImg.setPosition(0, 0);
	// --------------------------
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");


	
	sf::Text GameWinTxt;
	GameWinTxt.setFont(gameFont);
	GameWinTxt.setFillColor(sf::Color::Cyan);
	GameWinTxt.setString("Congratulations You Win");
	GameWinTxt.setCharacterSize(40);
	GameWinTxt.setPosition(550,305);
	sf::Text GameWinTxtsecLin;
	GameWinTxtsecLin.setFont(gameFont);
	GameWinTxtsecLin.setFillColor(sf::Color::Cyan);
	GameWinTxtsecLin.setString("I would like to thank Sanquinyx for letting me uses his artwork as a wallpaper");
	GameWinTxtsecLin.setCharacterSize(30);
	GameWinTxtsecLin.setPosition(350, 505);

	sf::Text GameWinTxtThirdLin;
	GameWinTxtThirdLin.setFont(gameFont);
	GameWinTxtThirdLin.setFillColor(sf::Color::Cyan);
	GameWinTxtThirdLin.setString("Music was Once Upon A Time ... Storybook Love by Mark Knopfler");
	GameWinTxtThirdLin.setCharacterSize(30);
	GameWinTxtThirdLin.setPosition(350, 705);


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))

	{
		MenuOpt = 0;
		LvlNumber = 1;
		
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))

	{
		window.close();
	}

	// THIS CLEAR SREEN AND ADD COLOUR
	window.clear(sf::Color(0, 0, 0));
	// Drawing BackGound Image 
	window.draw(BGImg);
	window.draw(GameWinTxt);
	window.draw(GameWinTxtsecLin);
	window.draw(GameWinTxtThirdLin);
	window.display();
}

void Game::Input()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		// Close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))

		{
			window.close();
		}

	}
	levelScreenInstance.Input();
}

void Game::Update()
{
	sf::Time frameTime = gameClock.restart();
	levelScreenInstance.Update(frameTime);
}

void Game::Draw()
{
	
	

	// THIS CLEAR SREEN AND ADD COLOUR
	window.clear(sf::Color(0,0,0));
	levelScreenInstance.DrawTo(window);
	window.display();



	

	

}

sf::RenderWindow & Game::GetWindow()
{
	return window;
}

int Game::GetMenuOpt()
{
	return MenuOpt;
}

void Game::setGameover()
{
	MenuOpt = 5;
	
}

void Game::SetGameWin()
{
	MenuOpt = 6;
}

void Game::SetNextLvl()
{
	LvlNumber++;
	levelScreenInstance.LoadLevel(LvlNumber);
}

