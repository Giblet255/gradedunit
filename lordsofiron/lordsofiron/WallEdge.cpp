#include "WallEdge.h"
#include "AssetManager.h"

WallEdge::WallEdge(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Edge2G.png"))
{
	sprite.setPosition(startingPos);
}

