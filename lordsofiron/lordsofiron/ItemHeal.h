#pragma once
#include "SpriteObject.h"
#include "Player.h"

class Game;

class ItemHeal : public SpriteObject
{
public:
	
	// Constructors / Destructors
	ItemHeal(sf::Vector2f startingPos);

	void HandleSolidCollision(Player& Playerobject);
	// Functions to call Player-specific code


	// Getter
	bool GetAlive();

	// Setter 
	void SetAlive(bool newAlive);



private:
	
	bool alive;
};