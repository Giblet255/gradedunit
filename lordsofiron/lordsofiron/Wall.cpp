#include "Wall.h"
#include "AssetManager.h"

Wall::Wall(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Wall2.png"))
{
	sprite.setPosition(startingPos);
}
