#pragma once
#include "AnimatingObject.h"
#include "player.h"

class BadGuyz : public AnimatingObject
{
public:
	// Constructors / Destructors
	BadGuyz(sf::Vector2f startingPos);
	
	// Functions to call Badguyz-specific code
	
	void Update(sf::Time frameTime, Player& playerObject);
	void HandleSolidCollision(Player& Playerobject);
	


	// Getter
	bool GetAlive();

	// Setter 
	void SetAlive(bool newAlive);
	
	
	
private:
	// Data
	sf::Vector2f velocity;
	float speed;
	sf::Vector2f previousPosition;
	// i want the walking patter to move for start loc of bad guyz
	sf::Vector2f locpointA;
	sf::Vector2f locpointB;
	sf::Vector2f locpointC;
	std::vector<sf::Vector2f> movementPattern;
	int currentInstruction;

	bool alive;

	sf::Time deathtimemax = (sf::seconds(5.0f));
	sf::Time deathtimecooldown = (sf::seconds(0.0f));
	
	
};
