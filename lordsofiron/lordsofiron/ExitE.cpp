#include "ExitE.h"
#include "AssetManager.h"
#include "Player.h"
#include "Game.h"



ExitE::ExitE(sf::Vector2f startingPos, Game* NewLvlPointer)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/ExitE.png"))
	, LvlNumber()
	, LvlPointer(NewLvlPointer)
{
	sprite.setPosition(startingPos);
}


void ExitE::HandleSolidCollision(Player& PlayerOjbect)

{
	bool isColliding = GetHitbox().intersects(PlayerOjbect.GetHitbox());
	// If there is a collision...
	if (isColliding)
	{
		// This is a pointer system you are using it to call a funtion in game This make the lvl Skip



		LvlPointer->SetGameWin();

	}

}
