#include "ExitT.h"
#include "AssetManager.h"
#include "Player.h"
#include "Game.h"



ExitT::ExitT(sf::Vector2f startingPos, Game* NewLvlPointer)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/ExitT.png"))
	, LvlNumber()
	, LvlPointer(NewLvlPointer)
{
	sprite.setPosition(startingPos);
}

void ExitT::HandleSolidCollision(Player& PlayerOjbect)
	
{
	bool isColliding = GetHitbox().intersects(PlayerOjbect.GetHitbox());
	// If there is a collision...
	if (isColliding)
	{
		// This is a pointer system you are using it to call a funtion in game This make the lvl Skip

		

		LvlPointer->SetNextLvl();

	}

}
