#pragma once
#include <SFML/Graphics.hpp>
#include "LevelScreen.h"

class Game
{
public:
	Game();
	void RunGameLoop();
	void Menu();
	void Option();
	void GameOver();
	void GameWin();
	void Input();
	void Update();
	void Draw();

	// ** NOTE FOR SCOTT YOU ARE SETTING A GETTER HERE 
	sf::RenderWindow& GetWindow();
	int GetMenuOpt();
	void setGameover();
	void SetGameWin();
	void SetNextLvl();

private:
	sf::RenderWindow window;
	sf::Clock gameClock;
	LevelScreen levelScreenInstance;
	int MenuOpt;
	int LvlNumber;
	




};

