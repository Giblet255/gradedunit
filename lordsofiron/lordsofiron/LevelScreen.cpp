#include "LevelScreen.h"
#include "Game.h"
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>


LevelScreen::LevelScreen(Game * newGamePointer)
	: playerInstance()
	, badguyzInstance()
	, gamePointer(newGamePointer)
	, wallInstances()
	, foorInstance()
	, WallEdgeInstance()
	, menuInstance()
	, Playgametxt()
	, camera(newGamePointer->GetWindow().getDefaultView())	
{
	
	
	
	// TODO: Calculate center of the screen
	sf::Vector2f newPosition;
	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;

	//playerInstance.SetPosition(newPosition);

	
	

}

void LevelScreen::Input()
{
	playerInstance.Input();
	
}

void LevelScreen::Update(sf::Time frameTime)
{
	
	playerInstance.Update(frameTime);
	
	

	for (int i = 0; i < ExitInstance.size(); ++i)
	{
			ExitInstance[i].HandleSolidCollision(playerInstance);

		
	}

	for (int i = 0; i < HealthpotInstance.size(); ++i)
	{
		HealthpotInstance[i].HandleSolidCollision(playerInstance);

		// If the Badguy is dead, delete it
		if (!HealthpotInstance[i].GetAlive())
		{
			// Remove the item from the vector
			HealthpotInstance.erase(HealthpotInstance.begin() + i);
		}


	}


	for (int i = 0; i < GameWinInstance.size(); ++i)
	{
		GameWinInstance[i].HandleSolidCollision(playerInstance);


	}


	
	
	for (int i = 0;i < badguyzInstance.size(); ++i)
	{
		badguyzInstance[i].Update(frameTime,playerInstance);
		badguyzInstance[i].HandleSolidCollision(playerInstance);
		playerInstance.HandleSolidCollision(badguyzInstance[i].GetHitbox());
		// If the Badguy is dead, delete it
		if (!badguyzInstance[i].GetAlive())
		{
			// Remove the item from the vector
			badguyzInstance.erase(badguyzInstance.begin() + i);
		}

	}
	

	for (int i = 0; i < wallInstances.size(); ++i)
	{
		playerInstance.HandleSolidCollision(wallInstances[i].GetHitbox());
	}

	for (int i = 0; i < TreeBInstance.size(); ++i)
	{
		playerInstance.HandleSolidCollision(TreeBInstance[i].GetHitbox());
	}

	

	if(playerInstance.GetPlayerHP()==0){
		gamePointer->setGameover(); // THIS *IS A POINTER YOU NEED IT !
	}


}

void LevelScreen::DrawTo(sf::RenderTarget & target)
{
	
	// Update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();
	sf::Vector2f Hudoffset;
	sf::Vector2f HPoffset;
	sf::Vector2f MBoffset;
	

		




	float playerCenterY = playerInstance.GetHitbox().top + playerInstance.GetHitbox().height / 2;
	float playerCenterX = playerInstance.GetHitbox().left + playerInstance.GetHitbox().width / 2;
	if (playerCenterY != currentViewCenter.y|| playerCenterX != currentViewCenter.x)
	{
		camera.setCenter(playerCenterX, playerCenterY);


		// this is temp to test bars Max Health is 255
		//int PlayerHP = 225;

		Hudoffset.x = playerCenterX - 950;
		Hudoffset.y = playerCenterY - 550;

		HPoffset.x = playerCenterX - 788;
		HPoffset.y = playerCenterY - 505;

		MBoffset.x = playerCenterX - 796;
		MBoffset.y = playerCenterY - 450;


		// this Setts the hud loc the Hud 

		hudInstance[0].SetPosition(Hudoffset);
		
		HpBack.setSize(sf::Vector2f(225, 40));
		HpBack.setFillColor(sf::Color::Red);
		HpBack.setPosition(HPoffset);


		rectangle.setSize(sf::Vector2f(playerInstance.GetPlayerHP(), 40));
		rectangle.setFillColor(sf::Color::Yellow);
		rectangle.setPosition(HPoffset);


		MBBar.setSize(sf::Vector2f(playerInstance.GetPlayerMagic(), 30));
		MBBar.setFillColor(sf::Color::Cyan);
		MBBar.setPosition(MBoffset);


		MBBack.setSize(sf::Vector2f(242, 30));
		MBBack.setFillColor(sf::Color::Blue);
		MBBack.setPosition(MBoffset);
	}
	
	
	// Set camera view
	target.setView(camera);
	
	// Draw objects 

	for (int i = 0; i < foorInstance.size(); ++i)
	{
		foorInstance[i].DrawTo(target);
	}

	for (int i = 0; i < MudInstance.size(); ++i)
	{
		MudInstance[i].DrawTo(target);
	}

	for (int i = 0; i < ExitInstance.size(); ++i)
	{
		ExitInstance[i].DrawTo(target);
	}

	for (int i = 0; i < HealthpotInstance.size(); ++i)
	{
		HealthpotInstance[i].DrawTo(target);
	}

	for (int i = 0; i < GameWinInstance.size(); ++i)
	{
		GameWinInstance[i].DrawTo(target);
	}

	for (int i = 0; i < TreeBInstance.size(); ++i)
	{
		TreeBInstance[i].DrawTo(target);
	}


	for (int i = 0; i < wallInstances.size(); ++i)
	{
		wallInstances[i].DrawTo(target);
	}
	
	for (int i = 0; i < WallEdgeInstance.size(); ++i)
	{
		WallEdgeInstance[i].DrawTo(target);
	}

	// Draw content to screen
	//badguyzInstance.DrawTo(target);

	for (int i = 0; i < badguyzInstance.size(); ++i)
	{
		badguyzInstance[i].DrawTo(target);
		

	}

	

	// Draw content to screen
	playerInstance.DrawTo(target);

	

	// Player will move Behind Tree Top 

	for (int i = 0; i < TreeTInstance.size(); ++i)
	{
		TreeTInstance[i].DrawTo(target);
	}
	
	// Trying to Draw Health bar

	target.draw(HpBack);
	target.draw(rectangle);
	// Drawing mana Bar 
	target.draw(MBBack);
	target.draw(MBBar);
	
	// Shows Hud for player
	

	for (int i = 0; i < hudInstance.size(); ++i)
	{

		hudInstance[i].DrawTo(target);

	}
	
	


	// This Draws the main menus System
	// menuInstance.DrawTo(target);
	
	


	
	// Remove camera view
	target.setView(target.getDefaultView());
}

void LevelScreen::LoadLevel(int levelNumber)
{
	// Figure out the file path based on the level number
	std::string filePath = "Assets/Levels/Level" + std::to_string(levelNumber) + ".txt";

	// Open the level file using this path
	std::ifstream inFile;
	inFile.open(filePath);
	if (!inFile)
	{
		std::cerr << "Unable to open file " + filePath;
		exit(1); // Call system to stop program with error
	}

	// Clear out the existing level to be ready to read the new one
	wallInstances.clear();
	foorInstance.clear();
	badguyzInstance.clear();
	hudInstance.clear();
	MudInstance.clear();
	TreeBInstance.clear();
	TreeTInstance.clear();
	WallEdgeInstance.clear();
	ExitInstance.clear();
	GameWinInstance.clear();
	HealthpotInstance.clear();

	// Read through the file, processing:
	// Set the starting x and y coordinates used to position level objects
	float x = 0.0f;
	float y = 0.0f;
	// Define the spacing we will use for our grid
	float xSpacing = 100.0f;
	float ySpacing = 100.0f;
	// Read each character one by one from the file...
	char ch;
	while (inFile >> std::noskipws >> ch)
	{
		// Perform actions based on what character was read in
		if (ch == ' ')
		{
			// New column, increase x
			x += xSpacing;
		}
		else if (ch == '\n')
		{
			// New row, increase y and set x back to 0
			y += ySpacing;
			x = 0;
		}
		else if (ch == 'P')
		{
			// Player (positioning the player)
			playerInstance.SetPosition(sf::Vector2f(x, y));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
			hudInstance.push_back(Hud(sf::Vector2f(x, y)));
			playerInstance.SetPlayerHP(225);

		}
		else if (ch == 'p')
		{
			// Player (positioning the player)
			playerInstance.SetPosition(sf::Vector2f(x, y));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
			hudInstance.push_back(Hud(sf::Vector2f(x, y)));
			playerInstance.SetPlayerHP(225); // *** This Reseting health 
		}


		else if (ch == 'B')
		{
			// Badguyz (positioning the Badguyz)
			
			badguyzInstance.push_back(BadGuyz(sf::Vector2f(x, y)));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));

		}
		else if (ch == 'b')
		{
			// Badguyz (positioning the Badguyz)
			badguyzInstance.push_back(BadGuyz(sf::Vector2f(x, y)));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}


		else if (ch == 'W')
		{
			// Walls (creating them, positioning them, and adding them to the vector)
			wallInstances.push_back(Wall(sf::Vector2f(x, y)));
		}

		else if (ch == 'w')
		{
			// Walls (creating them, positioning them, and adding them to the vector)
			wallInstances.push_back(Wall(sf::Vector2f(x, y)));
		}

		else if (ch == 'F')
		{
			// Floor (creating them, positioning them, and adding them to the vector)
			
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'f')
		{
			// Floor (creating them, positioning them, and adding them to the vector)

			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'T')
		{
			// Tree Top (creating them, positioning them, and adding them to the vector)

			TreeTInstance.push_back(TreeT(sf::Vector2f(x, y)));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 't')
		{
			// Tree Trunk This is a diffenrt thing (creating them, positioning them, and adding them to the vector)

			TreeBInstance.push_back(TreeB(sf::Vector2f(x, y)));
		}

		else if (ch == 'M')
		{
			// Floor With Shading (creating them, positioning them, and adding them to the vector)
			MudInstance.push_back(Mud(sf::Vector2f(x, y)));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'm')
		{
			// Floor With Shading (creating them, positioning them, and adding them to the vector)
			MudInstance.push_back(Mud(sf::Vector2f(x, y)));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}


		else if (ch == 'E')
		{
			// Walls (creating them, positioning them, and adding them to the vector)
			WallEdgeInstance.push_back(WallEdge(sf::Vector2f(x, y)));
		}

		else if (ch == 'x')
		{
			// NextLvl Portal (creating them, positioning them, and adding them to the vector)
			ExitInstance.push_back(ExitT(sf::Vector2f(x, y),gamePointer));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'X')
		{
		//  NextLvl Portal  (creating them, positioning them, and adding them to the vector)
			ExitInstance.push_back(ExitT(sf::Vector2f(x, y),gamePointer));
			foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'z')
		{
		// Ends Game Win (creating them, positioning them, and adding them to the vector)
		
		GameWinInstance.push_back(ExitE(sf::Vector2f(x, y), gamePointer));
		foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'Z')
		{
		// Ends Game Win (creating them, positioning them, and adding them to the vector)
		GameWinInstance.push_back(ExitE(sf::Vector2f(x, y), gamePointer));
		foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'h')
		{
		// Ends Game Win (creating them, positioning them, and adding them to the vector)

		HealthpotInstance.push_back(ItemHeal(sf::Vector2f(x, y)));
		foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}

		else if (ch == 'H')
		{
		// Ends Game Win (creating them, positioning them, and adding them to the vector)
		HealthpotInstance.push_back(ItemHeal(sf::Vector2f(x, y)));
		foorInstance.push_back(Floor(sf::Vector2f(x, y)));
		}



		else if (ch == 'e')
		{
			// Walls (creating them, positioning them, and adding them to the vector)
			WallEdgeInstance.push_back(WallEdge(sf::Vector2f(x, y)));
		}


		else if (ch == '-')
		{
			// Do nothing - empty space
		}
		else
		{
			// Anything else is unrecognised
			std::cerr << "Unrecognised character in level file: " << ch;
		}
	}
	// Close the file now that we are done with it
	inFile.close();
	

}

