#include "Floor.h"
#include "AssetManager.h"

	

Floor::Floor(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/FloorStone4.png"))
{
	sprite.setPosition(startingPos);
}
