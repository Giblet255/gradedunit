#include "ItemHeal.h"
#include "AssetManager.h"
#include "Player.h"
#include "Game.h"

ItemHeal::ItemHeal(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/healpot.png"))

{
	sprite.setPosition(startingPos);
	alive = true;
}


void ItemHeal::HandleSolidCollision(Player& Playerobject)
{

	bool isColliding = GetHitbox().intersects(Playerobject.GetHitbox());
	// If there is a collision...
	if (isColliding)
	{
		int playerHP;
		int NewplayerHp;


		playerHP = Playerobject.GetPlayerHP();

		NewplayerHp = playerHP + 45;

		Playerobject.SetPlayerHP(NewplayerHp);

		alive = false;

	}

}

bool ItemHeal::GetAlive()
{
	return alive;
}

void ItemHeal::SetAlive(bool newAlive)
{
	alive = newAlive;
}
