#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Badguyz.h"
#include "Wall.h"
#include "Floor.h"
#include "Mud.h"
#include "TreeT.h"
#include "TreeB.h"
#include "WallEdge.h"
#include "MenuSystem.h"
#include "PlayerKnife.h"
#include "ExitT.h"
#include "ExitE.h"
#include "ItemHeal.h"

class Game;

class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);
	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
	void LoadLevel(int levelNumber);
	

private:
	Player playerInstance;
    std::vector<BadGuyz> badguyzInstance;
	std::vector<Wall> wallInstances;
	std::vector<Floor> foorInstance;
	std::vector<Mud>MudInstance;
	std::vector<TreeT>TreeTInstance;
	std::vector<TreeB>TreeBInstance;
	std::vector<Hud>hudInstance;
	std::vector<WallEdge> WallEdgeInstance;
	std::vector<PlayerTXT>Playgametxt;
	std::vector<PlayerKnife>PlayerKnifeInstance;
	std::vector<ExitT>ExitInstance;
	std::vector<ExitE>GameWinInstance;
	std::vector<ItemHeal>HealthpotInstance;


	

	sf::RectangleShape rectangle;
	sf::RectangleShape HpBack;

	sf::RectangleShape MBBar;
	sf::RectangleShape MBBack;

	Menu menuInstance;
	
	

	Game* gamePointer;
	sf::View camera;


};

