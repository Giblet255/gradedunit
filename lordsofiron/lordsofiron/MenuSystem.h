#pragma once
#include "SpriteObject.h"
#include <SFML/Audio.hpp> // this load in the sound buffers



class Menu : public SpriteObject
{
public:
	// Constructors / Destructors
	Menu();
	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void SetPosition(sf::Vector2f newPosition);

	
	
private:
	
	sf::Music gameMusic;
	sf::Font gameFont;
	sf::Text Playgametxt;
	


};

class PlayerTXT : public SpriteObject
{
public:
	// Constructors / Destructors
	PlayerTXT(sf::Vector2f startingPos);
	// Functions to call Player-specific code

private:


};